#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/stat.h>

typedef struct header{
	char name[PATH_MAX];
	int size;
}header;
	 
int main(int argc, char* argv[])
{
	int sizeOfHeader = sizeof(header);
	struct stat st;
	if(argc == 3 && argv[1] == "-l"){
		puts("listing archived files.");
		/*
		   read first line to get the first filename and the size of that file
		   then seek forward to the end of that file plus one and repeat to end
		 */
		stat(argv[2], &st);
		int sizeOfArch = st.st_size;
		FILE* arch = fopen(argv[2], "r");
		char line[sizeof(header)];
		header* headerptr;
		int filesize;
		while(!feof(arch) && fgets(line, sizeof(line), arch)){
			printf("%s\n", line);
			parse(line, headerptr);
			filesize = headerptr->size;
			fseek(arch, 0, filesize, SEEK_CUR);
		}
	}else if(argc == 4 && argv[1] == "-a"){
		puts("adding to archive.");
		/*
		   same as list but, at end append a line stating the filename and the size
		   of the new file. then append the new file.
		 */
		 stat(argv[2], &st);
	 	int sizeOfArch = st.st_size;
	 	FILE* arch = fopen(argv[2], "r");
	 	char line[sizeof(header)];
	 	header* headerptr;
	 	int filesize;
	 	while(!feof(arch) && fgets(line, sizeof(line), arch)){
	 		printf("%s\n", line);
	 		parse(line, headerptr);
	 		filesize = headerptr->size;
	 		fseek(arch, 0, filesize, SEEK_CUR);
	 	}
		fwrite(headerptr, &arch);
		for(int i = 0; i < filesize; i++){
			fwrite(line, &arch);
		}
	}else if(argc == 4 && argv[1] == "-e"){
		puts("extracting from archive.");
		/*
		   while not EOF check if filename matches current file name at location
		   if so write new file made of contents and name of that file.
		 */
		 stat(argv[2], &st);
		 int sizeOfArch = st.st_size;
		 FILE* arch = fopen(argv[2], "r");
		 FILE* outputFile = fopen(argv[3], "w+");
		 char line[sizeof(header)];
		 header* headerptr;
		 int filesize;
		 while(!feof(arch) && fgets(line, sizeof(line), arch)){
			 if(headerptr->name != argv[3]){
				 	for(int i = 0; i < filesize; i++){
				 		fwrite(line, &outputFile);
					}
			 }
			 printf("%s\n", line);
			 parse(line, headerptr);
			 filesize = headerptr->size;
			 fseek(arch, 0, filesize, SEEK_CUR);
		 }
	}else{
		puts("Invalid arguments.")
		return 1;
	}
	return 0;
}
