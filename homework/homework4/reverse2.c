#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <string.h>

main(int argc, char *argv[]){
	struct stat s;
	int fd = open(argv[1], O_RDONLY);
	FILE* filename = fopen(argv[1], "ab+");
	fstat(fd, &s);
	int size = s.st_size;
	unsigned char *f = (char *) mmap (0, size, PROT_READ, MAP_SHARED, fd, 0);
	for(int x = size - 4; x >= 0; x--){
		write(fd, f[x], size);
	}  
	munmap(f, size);
	close(fd);
	return 0;
}
