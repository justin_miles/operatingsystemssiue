#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <string.h>

int reverse2(int argc, char *argv[]){
	struct timeval t1, t2;

	struct stat s;
	int fd = open(argv[1], O_RDONLY);
	FILE* filename = fopen(argv[1], "ab+");
	fstat(fd, &s);
	int size = s.st_size;
	unsigned char *f = (char *) mmap (0, size, PROT_READ, MAP_SHARED, fd, 0);

		int res = gettimeofday(&t1, NULL);
		assert(res==0);

	for(int x = size - 4; x >= 0; x--){
		write(fd, f[x], size);
	}

	res = gettimeofday(&t2, NULL);
	long elapsed = (t2.tv_sec-t1.tv_sec)*1000000 + t2.tv_usec-t1.tv_usec;
	printf("%ld time elapsed.\n", elapsed);

	munmap(f, size);
	close(fd);
	return 0;
}

long nanosec(struct timeval t){
	return ((t.tv_sec*1000000+t.tv_usec)*1000);
}

int main(int argc, char* argv[]){
	int runs = 10;
	for(int i = 0; i < runs; i++)
		reverse(argc, &argv);
	for(int i = runs; i >=0; i--)
		reverse2(argc, argv);
}

int reverse(int argc, char* argv[]){
	struct timeval t1, t2;

	FILE* infile = fopen(argv[1], "r+");
	FILE* outfile = fopen(argv[2], "w+");
	if(infile == NULL || outfile == NULL){
		puts("bad file");
	}
	fseek(infile, 0, SEEK_END);
	long size = ftell(infile);
	fseek(infile, 0, SEEK_SET);
	char* buffer = (char*)malloc(sizeof(char));
	char* outbuffer = (char*)malloc(sizeof(char));
	fread(buffer, sizeof(char), size, infile);

	int res = gettimeofday(&t1, NULL);
	assert(res==0);

	for(int i = 0; i < size-1; i ++){
		outbuffer[i] = buffer[size-2-i];
	}
	fwrite(outbuffer, sizeof(char), size, outfile);

	res = gettimeofday(&t2, NULL);
	long elapsed = (t2.tv_sec-t1.tv_sec)*1000000 + t2.tv_usec-t1.tv_usec;
	printf("%ld time elapsed.\n", elapsed);

	fclose(infile);
	fclose(outfile);
	return 0;
}
