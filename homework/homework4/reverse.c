#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
	FILE* infile = fopen(argv[1], "r+");
	FILE* outfile = fopen(argv[2], "w+");
	if(infile == NULL || outfile == NULL){
		puts("bad file");
	}
	fseek(infile, 0, SEEK_END);
	long size = ftell(infile);
	fseek(infile, 0, SEEK_SET);
	char* buffer = (char*)malloc(sizeof(char));
	char* outbuffer = (char*)malloc(sizeof(char));
	fread(buffer, sizeof(char), size, infile);
	for(int i = 0; i < size-1; i ++){
		outbuffer[i] = buffer[size-2-i];
	}
	fwrite(outbuffer, sizeof(char), size, outfile);
	fclose(infile);
	fclose(outfile);
	return 0;
}
