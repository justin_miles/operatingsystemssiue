/*
Write a C program that generates some predictable number of page faults.
Show that your program generates roughly the expected number of page faults in addition
to the page faults that normally occur. For example:
$ /usr/bin/time -v ./genpagefault 0
Command being timed: "./genpagefault 0"
User time (seconds): 0.00
System time (seconds): 0.00
Percent of CPU this job got: 50%
Elapsed (wall clock) time (h:mm:ss or m:ss): 0:00.00
Average shared text size (kbytes): 0
Average unshared data size (kbytes): 0
Average stack size (kbytes): 0
Average total size (kbytes): 0
Maximum resident set size (kbytes): 532
Average resident set size (kbytes): 0
Major (requiring I/O) page faults: 0
Minor (reclaiming a frame) page faults: 157 <<<<<<<<<<<<<<<<<<
Voluntary context switches: 5
Involuntary context switches: 1
Swaps: 0
File system inputs: 0
File system outputs: 0
Socket messages sent: 0
Socket messages received: 0
Signals delivered: 0
Page size (bytes): 4096
Exit status: 0
$ /usr/bin/time -v ./genpagefault 1000
Command being timed: "./genpagefault 1000"
User time (seconds): 0.00
System time (seconds): 0.00
Percent of CPU this job got: 72%
Elapsed (wall clock) time (h:mm:ss or m:ss): 0:00.01
Average shared text size (kbytes): 0
Average unshared data size (kbytes): 0
Average stack size (kbytes): 0
Average total size (kbytes): 0
Maximum resident set size (kbytes): 4516
Average resident set size (kbytes): 0
Major (requiring I/O) page faults: 0
Minor (reclaiming a frame) page faults: 1154 <<<<<<<<<<<<<<<
Voluntary context switches: 5
Involuntary context switches: 5
Swaps: 0
File system inputs: 0
File system outputs: 0
Socket messages sent: 0
Socket messages received: 0
Signals delivered: 0
Page size (bytes): 4096
Exit status: 0
Make sure to use /usr/bin/time -v to see the minor (15 points) and major (15 points)
page faults (the bash builtin does not have this capability), as shown in the example.
Minor page faults can be produced by allocating a large chunk of memory (not on the
stack, of course) then iterating across it in page-sized increments (run getconf PAGESIZE
to get the system’s page size). You’ll have to read something from each location to get a
page fault (set a char variable equal to whatever’s at the current location, for example).
To get major page faults, you’ll have to cause disk I/O, so mmap a large file and iterate
across it in the same way as you will for minor page faults
*/

/*
Profile the overhead of minor and major page faults, by performing timings
as shown below, and compare this overhead to memory accesses without a page fault.
Report the average of a number of trials.
*/
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
// you may use this to convert the contents of the timeval struct to ns
long nanosec(struct timeval t){
  return((t.tv_sec*1000000+t.tv_usec)*1000);
}
int main(){
int res;
struct timeval t1, t2;
  res=gettimeofday(&t1,NULL); assert(res==0);
  //stuff you want to measure might go here
  res=gettimeofday(&t2,NULL); assert(res==0);
  //find average time here
}
