/*
Implement semaphores using the pthread library. Consult the thread implementation
in DLXOS for inspiration (semaphores are implemented in synch.h and synch.c).
Use pthread_mutex_lock and pthread_mutex_unlock (see man pthread mutex lock) to make
the semaphore wait and post operations atomic.
Use pthread_cond_wait (see man pthread cond timedwait, but don’t use pthread cond timedwait)
and pthread_cond_signal (see man pthread cond signal).
Test your solution by implementing a simple rendezvous, e.g.:
THREAD A:                 THREAD B:
printf(‘‘a1\n’’);         printf(‘‘b1\n’’);
sem_signal(aArrived);     sem_signal(bArrived);
sem_wait(bArrived);       sem_wait(aArrived);
printf(‘‘a2\n’’);         printf(‘‘b2\n’’);
Your program should consistently print a1 and b1 before printing a2 and b2.
*/
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

void main(){
  pthread_t threada, threadb;
  void *threadAFunction(), *threadBFunction();
  pthread_create(&threada, NULL, *threadAFunction, NULL);
  pthread_create(&threadb, NULLm *threadBFunction, NULL);

  return 0;
}

void *threadAFunction(){
  printf("a1\n");
  sem_signal(aArrived);
  sem_wait(bArrived);
  printf("a2\n");
  pthread_exit(0);
}

void *threadBFunction(){
  printf("b1\n");
  sem_signal(bArrived);
  sem_wait(aArrived);
  printf("b2\n");
  pthread_exit(0);
}
