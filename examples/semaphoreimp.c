/*
Implement semaphores using the pthread library. Consult the thread implementation
in DLXOS for inspiration (semaphores are implemented in synch.h and synch.c).
Use pthread_mutex_lock and pthread_mutex_unlock (see man pthread mutex lock) to make
the semaphore wait and post operations atomic.
Use pthread_cond_wait (see man pthread cond timedwait, but don’t use pthread cond timedwait)
and pthread_cond_signal (see man pthread cond signal).
Test your solution by implementing a simple rendezvous, e.g.:
THREAD A:                 THREAD B:
printf(‘‘a1\n’’);         printf(‘‘b1\n’’);
sem_signal(aArrived);     sem_signal(bArrived);
sem_wait(bArrived);       sem_wait(aArrived);
printf(‘‘a2\n’’);         printf(‘‘b2\n’’);
Your program should consistently print a1 and b1 before printing a2 and b2.
 * 
 * --Justin's personal notes--
 * I need two semaphores that can wait and post.
 * pthreads 
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

//mutex
pthread_mutex_t aarrived, barrived;

//condition
pthread_cond_t lock;
//hello

int islocked = 0;

void main() {
    pthread_cond_init(&lock, NULL);
    pthread_mutex_init(&aarrived, NULL);
    pthread_mutex_init(&barrived, NULL);

    pthread_t threada, threadb;
    void *threadAFunction(), *threadBFunction();
    
    pthread_create(&threada, NULL, *threadAFunction, NULL);
    pthread_create(&threadb, NULL, *threadBFunction, NULL);

    pthread_join(threada, NULL);
    pthread_join(threadb, NULL);
}

void sem_wait(pthread_mutex_t *mutex) {
    pthread_mutex_lock(mutex);
    while (islocked != 0) {
        islocked = pthread_cond_wait(&lock, mutex);
    }
    pthread_mutex_unlock(mutex);
}

void sem_signal(pthread_mutex_t *mutex) {
    pthread_mutex_unlock(mutex);
    pthread_cond_signal(&lock);
}

void *threadAFunction() {
    puts("a1");
    sem_signal(&aarrived);
    sem_wait(&barrived);
    puts("a2");
    pthread_exit(0);
}

void *threadBFunction() {
    puts("b1");
    sem_signal(&barrived);
    sem_wait(&aarrived);
    puts("b2");
    pthread_exit(0);
}
