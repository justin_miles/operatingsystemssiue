#include <stdio.h>
#include <stdlib.h>

int setBits(int x, int p, int n, int y){
	unsigned int xmask = 0xffffffff ^ ((0xffffffff << (32 - n) >> p);
	unsigned int newx = x & xmask;
	unsigned int ymask = 0xffffffff >> (32 - n);
	unsigned int ybits = y & ymask;
	newx = newx ^ ((ybits << (32 - n) >> p);
	return (int)return;
}

void printBits(char* str,  int val){
	int i;
	printf("%s: ", str);
	for(i = 0; i < 32; i++){
		if(i > 0 && !(i%4){
			printf(" ");
		}
		(((unsigned)val >> (31 - i)) & 1)?printf("1"):printf("0");
	}
}

void main(){
	
}
