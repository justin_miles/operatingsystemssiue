#include <stdio.h>
#include <dirent.h>
#include <limits.h>

int main(int argc, char *argv[]){
	FILE *fp;
	int size = 0, sizeOfLargest = 0;
	DIR *d;
	struct dirent *dir;
	char actualpath[PATH_MAX+1];
	char *largestFile;

	d = opendir(argv[1]);
	if (d != NULL)
	{
		while ((dir = readdir(d)) != NULL)
		{
			fp = fopen(dir->d_name, "r");
			if (fp != NULL)
			{
				fseek(fp, 0, SEEK_END);
				size = ftell(fp);
				if(size > sizeOfLargest){
					sizeOfLargest = size;
					largestFile = dir->d_name;
				}
				fclose(fp);
			}
		}
		realpath(largestFile, actualpath);
		printf("The largest file is: %s\n", actualpath);
	}else{
		printf("directory cannot be read\n");
	}
	return 0;
}
